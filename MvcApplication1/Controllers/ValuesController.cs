﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            string name;
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            string name;
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
            string name;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
            string name;
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            string name;
        }
    }
}